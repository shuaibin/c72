<?php namespace wechat;
/**
 * 微信基础类
 */
class Wx
{
	protected $config;
	//构造方法， array $config 类型约束，来了必须的是数组
	public function __construct( array $config )
	{
		//print_r($config);
		$this->config = $config;
	}

	//验证微信，进行通信验证
	public function valid()
	{
		//echo 122222;die;
		$signature = $_GET["signature"];
		$timestamp = $_GET["timestamp"];
		$nonce = $_GET["nonce"];

		$token = $this->config['token'];
		$tmpArr = [ $token , $timestamp , $nonce ];
		sort( $tmpArr , SORT_STRING );
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );

		if( $tmpStr == $signature ) {
			echo $_GET['echostr'];

			return true;
		} else {
			return false;
		}
	}
}