<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/14
 * Time: 9:04
 */

namespace wechat\build;


use wechat\Wx;

class Material extends Wx
{
	//http请求方式：POST/FORM
	//临时素材
	//https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE
	//永久素材
	//https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN&type=TYPE
	//上传素材文件
	/**
	 * @param     $file   上传的文件
	 * @param string $type  文件类型
	 * @param int $material  0临时   1永久
	 */
	public function upload($file,$type='image',$material=0)
	{
		switch($material){
			case 0:
				//临时
				$url = self::$config['apiUrl']."/cgi-bin/media/upload?access_token=".$this->getAccessToken()."&type=" . $type;
				break;
			default:
				//永久
				$url = self::$config['apiUrl']. "/cgi-bin/material/add_material?access_token=".$this->getAccessToken()."&type=".$type;
				break;
		}
		//执行curl请求
		//new \CURLFile()  5.5
		//@
		//将文件路径转为绝对路径
		$file = realpath($file);
		//false 不需要走__autoload
		if(class_exists('\CURLFile',false)){
			$data = [
				'media'=>new \CURLFile($file),
			];
		}else{
			$data = [
				'media'=> '@' . $file,
			];
		}
		//print_r($data);die;
		$jsonData = $this->curl($url,$data);
		return $this->get(json_decode($jsonData,true));
	}
}