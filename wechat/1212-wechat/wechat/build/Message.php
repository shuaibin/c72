<?php namespace wechat\build;
use wechat\Wx;

/**
 * 消息处理类
 * Class Message
 *
 * @package wechat\build
 */
class Message extends Wx
{
	#---------普通消息---------#
	//微信手册--消息管理--接受普通消息
	const MSG_TYPE_TEXT='text';//文本消息
	const MSG_TYPE_IMAGE='image';//图片消息
	const MSG_TYPE_VOICE='voice';//语音消息
	const MSG_TYPE_VIDEO='video';//视频消息
	const MSG_TYPE_SHORT_VIDEO='shortvideo';//小视频
	const MSG_TYPE_LOCATION='location';//地理位置
	const MSG_TYPE_LINK='link';//链接消息
	#---------事件消息---------#
	const EVENT_TYPE_SUBSCRIBE='subscribe';//关注
	const EVENT_TYPE_UNSUBSCRIBE='unsubscribe';//关注
	#---------事件消息判断---------#
	public function isSubscribeEvent()
	{
		return $this->message->MsgType=='event' && $this->message->Event==self::EVENT_TYPE_SUBSCRIBE;
	}
	public function isUnSubscribeEvent()
	{
		return $this->message->MsgType=='event' && $this->message->Event==self::EVENT_TYPE_UNSUBSCRIBE;
	}
	#---------普通消息判断---------#
	public function isLinkMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_LINK;
	}
	public function isLocationMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_LOCATION;
	}
	public function isShortVideoMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_SHORT_VIDEO;
	}
	public function isVideoMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_VIDEO;
	}
	public function isVoiceMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_VOICE;
	}
	public function isTextMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_TEXT;
	}
	public function isImageMsg()
	{
		return $this->message->MsgType==self::MSG_TYPE_IMAGE;
	}

	//给粉丝回复消息
	public function show($content)
	{
		$str = "<xml>
				<ToUserName><![CDATA[%s]]></ToUserName>
				<FromUserName><![CDATA[%s]]></FromUserName>
				<CreateTime>%s</CreateTime>
				<MsgType><![CDATA[text]]></MsgType>
				<Content><![CDATA[%s]]></Content>
				</xml>";
		$text = sprintf($str,$this->message->FromUserName,$this->message->ToUserName,time(),$content);
		echo $text;
	}
}