<?php namespace wechat\build;

use wechat\Wx;

/**
 * 粉丝管理
 * Class User
 *
 * @package wechat\build
 */
class User extends Wx
{
	public function getUserInfo( $openid , $lang = 'zh_CN' )
	{
		//http请求方式: GET
		$url = self::$config['apiUrl'] . "/cgi-bin/user/info?access_token=" . $this->getAccessToken() . "&openid=" . $openid . "&lang=" . $lang;
		$jsonData = $this->curl( $url );

		return $this->get( json_decode( $jsonData , true ) );
	}

	//批量获取粉丝信息
	public function getUserList( $data , $lang = 'zh_CN' )
	{
		//http请求方式: POST
		$url = self::$config['apiUrl'] . "/cgi-bin/user/info/batchget?access_token=" . $this->getAccessToken();
		//数组重组
		$user = [ ];
		foreach( $data as $openid ) {
			$user['user_list'][] = [
				'openid'=>$openid,
				'lang'=>$lang
			];
		}
//		echo '<pre>';
//		print_r($user);
		$jsonData = $this->curl( $url , json_encode($user,JSON_UNESCAPED_UNICODE) );
		return $this->get(json_decode($jsonData,true));
	}
}