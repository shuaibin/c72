<?php namespace wechat\build;

use wechat\Wx;

/**
 * 按钮操作类
 * Class Button
 *
 * @package wechat\build
 */
class Button extends Wx
{
	//创建按钮
	public function create( $data )
	{

		//echo $this->getAccessToken();
		//http请求方式：POST
		$url = self::$config['apiUrl'] . "/cgi-bin/menu/create?access_token=" . $this->getAccessToken();
		$jsonData = $this->curl( $url , $data );
		//请求get方法处理错误信息，转为中文
		return $this->get(json_decode($jsonData,true));
	}
	//删除菜单
	public function flush()
	{
		//echo self::$config['apiUrl'];die;
		//http请求方式：GET
		$url = self::$config['apiUrl']."/cgi-bin/menu/delete?access_token=" . $this->getAccessToken();
		$jsonData = $this->curl($url);
		return $this->get(json_decode($jsonData,true));
	}
}