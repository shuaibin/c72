<?php namespace wechat;

/**
 * 微信基础类
 */
class Wx extends Error
{
	protected static $config;//网站配置
	protected $message;//粉丝发送来的消息

	//构造方法， array $config 类型约束，来了必须的是数组
	public function __construct( array $config = [ ] )
	{
		if( !self::$config ) {
			self::$config = $config;
		}
		$this->message = $this->responseMessage();
	}

	//验证微信，进行通信验证
	public function valid()
	{
		if( isset( $_GET["signature"] ) && isset( $_GET["timestamp"] ) && isset( $_GET["nonce"] ) && isset( $_GET["echostr"] ) ) {
			//file_put_contents( './a.php' , 1 );
			$signature = $_GET["signature"];
			$timestamp = $_GET["timestamp"];
			$nonce = $_GET["nonce"];

			$token = self::$config['token'];
			$tmpArr = [ $token , $timestamp , $nonce ];
			sort( $tmpArr , SORT_STRING );
			$tmpStr = implode( $tmpArr );
			$tmpStr = sha1( $tmpStr );

			if( $tmpStr == $signature ) {
				echo $_GET['echostr'];
				exit;
			} else {
				return false;
			}
		}
	}

	//粉丝发送来的消息
	public function getMessage()
	{
		return $this->responseMessage();
	}

	//接受粉丝发送的消息
	protected function responseMessage()
	{
		if( $GLOBALS['HTTP_RAW_POST_DATA'] ) {
			return simplexml_load_string( $GLOBALS['HTTP_RAW_POST_DATA'] , 'simpleXmlElement' , LIBXML_NOCDATA );
		}
	}

	//实例化我们的功能类，例如消息恢复
	public function instance( $className )
	{
		//echo $className;
		//进行类的实例化【\wechat\build\Message】
		$class = '\wechat\build\\' . ucfirst( $className );

		return new $class();
		//$obj = new $class;return $obj;相当于上一句话
	}

	/**
	 * curl请求
	 *
	 * @param       $url        请求地址
	 * @param array $postData   请求数据
	 *
	 * @return string            返回数据
	 */
	public function curl( $url , $postData = [ ] )
	{
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_URL , $url );//curl请求地址
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , 1 );//不让你直接输出，返回变量接受
		//禁止校验
		curl_setopt( $ch , CURLOPT_SSL_VERIFYPEER , false );
		curl_setopt( $ch , CURLOPT_SSL_VERIFYHOST , false );

		if( $postData ) {
			curl_setopt( $ch , CURLOPT_TIMEOUT , 30 );//超时
			curl_setopt( $ch , CURLOPT_POST , 1 );//post
			curl_setopt( $ch , CURLOPT_POSTFIELDS , $postData );//传输数据
		}
		//执行
		if( curl_exec( $ch ) ) {
			//获取curl请求结果
			$data = curl_multi_getcontent( $ch );
		} else {
			$data = '';
		}
		//关闭curl
		curl_close( $ch );

		return $data;
	}

	//获取access_token
	public function getAccessToken()
	{

		//文件名称
		$fileName = md5( self::$config['appid'] . self::$config['appsecret'] ) . '.php';
		//文件完成路径
		$file = __DIR__ . '/cache/' . $fileName;
		if( is_file( $file ) && filemtime( $file ) + 7000>time() ) {//如果缓存没有过期，那么读取缓存
			$access_token = include $file;
		} else {//如果缓存过期，重新设置缓存
			//http请求方式: GET
			$url = self::$config['apiUrl']."/cgi-bin/token?grant_type=client_credential&appid=" . self::$config['appid'] . "&secret=" . self::$config['appsecret'];
			//返回的是json数据
			$jsonData = $this->curl( $url );
			//将结果转为数组
			$access_token = json_decode( $jsonData , true );
			//生成缓存文件
			file_put_contents( $file , "<?php return " . var_export( $access_token , true ) . " ;?>" );
		}
		return $access_token['access_token'];
	}
}