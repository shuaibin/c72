<?php
//1.yum -y install ntp
//启动 service ntpd start

//服务
/etc/init.d/httpd start
service httpd restart
//设置在345级别启动
chkconfig --level 345 httpd on
//查看
chkconfig --list httpd

//mysql
启动mysql   service mysqld start
mysql -uroot -p

//设置mysql密码:
mysql_secure_installation【全部执行y】

//查看有无partition插件,在mysql里面运行
show plugins;



//tange分区
create table pt(id int,user varchar(30))
partition BY RANGE (id) (
partition p0 VALUES LESS THAN (1000),
partition p1 VALUES LESS THAN (2000),
partition p2 VALUES LESS THAN (3000),
partition p3 VALUES LESS THAN MAXVALUE
);

\d $$

create procedure insert_data()
begin
declare i int default 5000;
while i>0 do
insert into pt(id,user)values(i,left(md5(i),5));
set i=i-1;
end while;
end;
$$
\d;
call insert_data();

//list分区
create table part(cid int,cname varchar(100))
partition BY LIST (cid) (
partition p1  VALUES IN (1,2,3),
partition p2 VALUES IN (4,5,6)
);

//hash分区
create table test(id int,name varchar(100),addtime date)
partition BY HASH (YEAR(addtime))
PARTITIONS 10;


//key分区
create table test(id int,name varchar(100),addtime date) engine myisam charset utf8
partition BY KEY (addtime)
PARTITIONS 10;



















