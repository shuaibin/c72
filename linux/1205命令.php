<?php
//git地址
https://git.oschina.net/wub/c72.git

//查看目录
ls
//显示全部
ls -a
//查看详细信息
ls -l
//更人性化的显示。单位（K,M）
ls -lh

//两次tab键自动补全

//退出当前命令
ctrl+c
//停止当前应用
ctrl+d


//关机（halt）
shutdown –h now  halt
//15：30关机
shutdown –h 15:30
//30 分钟后关机
shutdown –h +30
//20分钟之后重启 提示信息：系统将要重启
shutdown –r +20 "System will reboot"

//重启
reboot


//pwd显示当前工作路径
//passwd 修改密码


//创建目录
mkdir houdunwang
mkdir -p a/b/c //递归创建
//创建文件
touch index.php
vi b.php

//rm 删除目录或文件
rm index.php
rm -r houdunwang
//不需要确认，直接删除
rm –rf houdunwang

//复制文件
cp a.php houdun/a.php
cp *.php houdun


//mv 移动或者是更名，对文件或目录进行操作
mv houdun hd  //更名
mv a.php houdun //将a.php移动到houdun目录里面


//编辑文件
//1.vi index.php
//2.按i进入编辑
//3.输入内容
//4.按esc
//5.shift+;【之后输入set nu 会显示行号】
//6.wq(x)保存退出（q!不保存退出）

//查看内容
//查看文件前两行的内容
head -n 2 index.php
//查看文件后两行内容
tail -n 2 index.php
//查看全部内容
cat index.php
cat -n index.php//连带行号显示全部内容

//查找文件
find / -name index.php
//查找文件大小超过10000k的文件
find / -size +10000k










































































































