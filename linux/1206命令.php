<?php
//压缩
         //压缩完成文件名称    被压缩的文件
tar -zcvf houdunwang.tar.gz houdunwang
//解压
tar -zxvf houdunwang.tar.gz houdunwang

//vi 编辑器
//vi index.php
//按i键
//写入内容
//esc【nG光标移动到对应的行 G光标会到最后一行】
//shift+;==:【这一步之后set nu显示行号】【使用:/word向下搜索或者:?word向上搜索】【dd剪切，ndd向下剪切n行；yy复制，p黏贴】
//wq(x)保存退出。q!不保存退出


//配置网络
cd /etc/sysconfig/network-scripts/
//备份
cp ifcfg-eth0 ifcfg-eth0.bak
//编辑
vi ifcfg-eth0
//修改
ONBOOT=yes
BOOTPROTO=static
//在后边加上
NETMASK=255.255.255.0
IPADDR=192.168.21.123
GATEWAY=192.168.1.1
DNS1=8.8.8.8

//关闭selinux
cd /etc/selinux
//备份
cp config config.bak
//修改
vi config
将SELINUX=enforcing修改为SELINUX=disabled

//关闭防火墙
service iptables stop

//重启网络
service network restart
ping baidu.com


//xshell远程链虚接虚拟主机
ssh root@192.168.0.123

//添加账号
useradd hdwb
passwd hdwb

//指定用户组[添加一个hd账号，指定houdunwang组给他]
useradd -G houdunwang hd

//用户删除
userdel ceshi;
//连同家目录一起删除
 userdel –r hdxj

//创建用户组
groupadd houdunwang
//删除用户组
groupdel houdunwang


//指定用户组
usermod -G chaoren hdwb

//查看用户【500】
tail -n 10 /etc/passwd
//查看账号信息
id hdxj


//修改用户所有者
chown hdwb houdunwang

//修改用户所属组
chown :hdwb houdunwang
//修改文件以及子文件所有这和所属组
chown -R houdunwang:houdunwang hdwb


//切换用户
su lisi


