<?php namespace Addons\Base;
use Addons\Base\Model\BaseTextModel;
use Addons\Module;

/**
 * 后台
 * Created by PhpStorm.
 * User: 武斌
 * Date: 2016/12/14
 * Time: 10:45
 */
class Site extends Module
{
	public function send()
	{
		if(IS_POST)
		{
			$this->store(new BaseTextModel(),I('post.'));
		}
		//获取数据库数据
		$field= (new BaseTextModel())->find();
		$this->assign('field',$field);
		//$this->display('Addons/Base/View/send.html');
		//调用我们自定义的display方法
		$this->make();
	}
}