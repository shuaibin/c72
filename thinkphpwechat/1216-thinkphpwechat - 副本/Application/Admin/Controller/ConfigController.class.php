<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/14
 * Time: 11:33
 */

namespace Admin\Controller;


use Common\Controller\AdminController;
use Common\Model\ConfigModel;

class ConfigController extends AdminController
{
	public function __init()
	{
	}
	public function set()
	{
		if(IS_POST)
		{
			$this->store(new ConfigModel(),I('post.'));
		}
		//获取数据
		$field = m('config')->find(1);
		$this->assign('field',$field);
		$this->display();
	}
}