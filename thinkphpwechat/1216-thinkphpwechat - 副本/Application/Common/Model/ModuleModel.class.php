<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/16
 * Time: 9:51
 */

namespace Common\Model;


class ModuleModel extends BaseModel
{
	protected $pk ='id';
	protected $tableName = 'module';
	protected $_validate = [
		['title','require','请输入模块中文名称'],
		['name','require','请输入模块标识'],
		['actios','require','请输入模块动作哟'],
	];
	//添加

}