<?php
/**
 * 打印函数
 *
 * @param $data
 */
function dd( $data )
{
	echo '<pre>';
	print_r( $data );
	echo '</pre>';
}

/**
 * 全局变量
 *
 * @param        $name  变量名
 * @param string $value 变量值
 *
 * @return mixed 返回值
 *               v('a','hodunwang')
 *               v('a')
 */
if( !function_exists( 'v' ) ) {
	function v( $name = null , $value = '[null]' )
	{
		static $vars = [ ];
		if( is_null( $name ) ) {
			return $vars;
		} else if( $value == '[null]' ) {
			//取变量
			$tmp = $vars;
			foreach( explode( '.' , $name ) as $d ) {
				if( isset( $tmp[ $d ] ) ) {
					$tmp = $tmp[ $d ];
				} else {
					return null;
				}
			}

			return $tmp;
		} else {
			//设置
			$tmp = &$vars;
			foreach( explode( '.' , $name ) as $d ) {
				if( !isset( $tmp[ $d ] ) ) {
					$tmp[ $d ] = [ ];
				}
				$tmp = &$tmp[ $d ];
			}

			return $tmp = $value;
		}
	}
}
/**
 * 模块后台跳转函数
 * @param       $url 跳转地址
 * @param array $args 参数
 */
function site_url( $url , $args = [ ] )
{
	//查看定义的常量
		//$d = get_defined_constants('true');
		//dd($d['user']);
	$info = explode('.',$url);
	//dd($url);dd($args);
	return __APP__ . '?mo='.$info[0].'&ac='.$info[1].'&t=Site&' . http_build_query($args);
}
/**
 * 模块前台跳转函数
 * @param       $url 跳转地址
 * @param array $args 参数
 */
function web_url( $url , $args = [ ] )
{
	//查看定义的常量
	//$d = get_defined_constants('true');
	//dd($d['user']);
	$info = explode('.',$url);
	//dd($url);dd($args);
	return __APP__ . '?mo='.$info[0].'&ac='.$info[1].'&t=Web&' . http_build_query($args);
}