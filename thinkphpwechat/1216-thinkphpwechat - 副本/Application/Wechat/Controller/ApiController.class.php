<?php namespace Wechat\Controller;

use Common\Controller\BaseController;
use wechat\WeChat;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 10:11
 */
class ApiController extends BaseController
{
	public function __init()
	{
		( new WeChat() )->valid();
	}

	public function handler()
	{
		//(new \Addons\Base\Processer)->handler();
		$this->responceMessage( 'base' );
	}

	protected function responceMessage( $module = '' )
	{
		$class = 'Addons\\' . ucfirst($module) . '\Processer';
		call_user_func_array( [ new $class , 'handler' ] , [ ] );
	}
}