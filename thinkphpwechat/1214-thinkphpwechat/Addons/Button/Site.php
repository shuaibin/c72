<?php namespace Addons\Button;

use Addons\Button\Model\ButtonModel;
use Addons\Module;
use wechat\WeChat;

/**
 * 后台
 * Created by PhpStorm.
 * User: 武斌
 * Date: 2016/12/14
 * Time: 10:45
 */
class Site extends Module
{
	public function lists()
	{
		//获取所有数据
		$field = ( new ButtonModel() )->select();
		//dd($field);
		$this->assign( 'field' , $field );
		$this->make();
	}

	//菜单添加
	public function addButton()
	{
		$id = I( 'get.id' );
		if( IS_POST ) {
			if( $id ) {
				$data = I( 'post.' );
				$data['id'] = $id;
				$data['status'] = 0;
			}
			$this->store( new ButtonModel() , $data , function () {
				$this->success( '操作成功' , site_url( 'Button.lists' ) );
				exit;
			} );
		}
		//获取旧数据
		if( $id ) {
			$field = ( new ButtonModel() )->find( $id );
			$this->assign( 'field' , $field );
		} else {
			//解决添加的时候点击添加一级菜单报错：方式二
			$this->assign( 'field' , [ 'content' => "{'button':[]}" ] );
		}
		$this->make();
	}

	//菜单删除
	public function delButton()
	{
		$id = I( 'get.id' );
		if( ( new ButtonModel() )->where( "id={$id}" )->delete() ) {
			$this->success( '操作成功' );
		} else {
			$this->error( '操作失败' );
		}

	}

	//菜单推送
	public function push()
	{
		$id = I( 'get.id' );
		//在数据库获取对应菜单数据
		$button = ( new ButtonModel() )->find( $id );
		$res = ( new WeChat() )->instance( 'button' )->create( $button['content'] );
		if( $res == 1 ) {
			//如果推送成功
			//修改状态
			( new ButtonModel() )->where( "id={$id}" )->save( [ 'status' => 1 ] );
			//更新其他数据状态为未为推送[<>不等于]
			( new ButtonModel() )->where( "id<>{$id}" )->save( [ 'status' => 0 ] );
			//成功提示
			$this->success( '操作成功' );
			exit;
		} else {
			//推送失败
			$this->error( $res['errmsg'] );
			exit;
		}
	}
}