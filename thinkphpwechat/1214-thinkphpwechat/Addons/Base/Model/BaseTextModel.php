<?php namespace Addons\Base\Model;
use Common\Model\BaseModel;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/16
 * Time: 11:12
 */
class BaseTextModel extends BaseModel
{
	protected $pk = 'id';
	protected $tableName = 'base_text';
	protected $_validate = [
		['system','require','请输入关注回复信息'],
		['default','require','请输入关注默认回复'],
	];
	public function store($data){
		if($textData = $this->find()){
			//如果能在数据库找到数据。那么执行编辑
			$data[$this->pk] = $textData['id'];
		}
		return parent::store($data);
	}
}