<?php namespace Addons\News;

use Addons\Module;
use Addons\News\Model\ArticleModel;
use Addons\News\Model\CategoryModel;

/**
 * 后台
 * Created by PhpStorm.
 * User: 武斌
 * Date: 2016/12/14
 * Time: 10:45
 */
class Site extends Module
{
	//栏目展示页面
	public function category()
	{
		//读取数据
		$field = ( new CategoryModel() )->select();
		$this->assign( 'field' , $field );
		$this->make();
	}

	//栏目添加
	public function addCate()
	{
		$id = I( 'get.id' );
		if( IS_POST ) {
			$data = I( 'post.' );
			if( $id ) {
				$data['id'] = $id;
			}
			$this->store( new CategoryModel() , $data , function () {
				$this->success( '操作成功' , site_url( 'news.category' ) );
				exit;
			} );
		}
		//获取编辑旧数据
		if( $id ) {
			$field = ( new CategoryModel() )->find( $id );
			$this->assign( 'field' , $field );
		}
		$this->make();
	}

	//栏目删除
	public function delCate()
	{
		$id = I( 'get.id' );
		if( ( new CategoryModel() )->where( "id={$id}" )->delete() ) {
			$this->success( '操作成功' );
		} else {
			$this->error( '操作失败' );
		}
	}

	//文章展示页面
	public function article()
	{
		$field = ( new ArticleModel() )->select();
		$this->assign( 'field' , $field );
		$this->make();
	}

	//文章添加
	public function addArticle()
	{
		$id = I( 'get.id' );
		if( IS_POST ) {
			$data = I( 'post.' );
			if( $id ) {
				$data['id'] = $id;
			}
			$this->store( new ArticleModel() , $data , function () {
				$this->success( '操作成功' , site_url( 'News.article' ) );
				exit;
			} );
		}
		//获取旧数据
		if( $id ) {
			$field = ( new ArticleModel() )->find( $id );
			$this->assign( 'field' , $field );
		}
		$this->make();
	}

	//删除文章
	public function delArticle()
	{
		$id = I( 'get.id' );
		if( ( new ArticleModel() )->where( "id={$id}" )->delete() ) {
			$this->success('操作成功');
		}else{
			$this->error('操作失败');
		}
	}
}