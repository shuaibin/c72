<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/18
 * Time: 10:37
 */

namespace Addons\News\Model;


use Common\Model\BaseModel;

class ArticleModel extends BaseModel
{
	protected $pk = 'id';
	protected $tableName = 'news_article';
	protected $_validate =[
		['title','require','请填文章标题'],
		['content','require','请填文章内容'],
		['click','require','请填点击次数'],
	];
}