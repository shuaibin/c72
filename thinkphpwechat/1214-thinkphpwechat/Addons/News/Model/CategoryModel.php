<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/18
 * Time: 10:37
 */

namespace Addons\News\Model;


use Common\Model\BaseModel;

class CategoryModel extends BaseModel
{
	protected $pk = 'id';
	protected $tableName = 'news_cate';
	protected $_validate =[
		['cname','require','请填写栏目名称'],
	];
}