<?php
/**
 * 关键词处理
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/17
 * Time: 8:59
 */

namespace Addons\Text;


use Addons\Module;
use Addons\Text\Model\TextContentModel;

class Keywords extends Module
{
	public function set($kid='')
	{
		if($kid)
		{
			//获取旧数据
			$oldData = (new TextContentModel())->where("kid={$kid}")->find();
			$this->assign('oldData',$oldData);
			//dd($oldData);
		}
		return $this->fetch('Addons/Text/View/set.html');
	}
	//添加关键词回复、修改
	public function submit($kid)
	{
		//执行自己模块数据表的添加 c72_text_content
		if($kid){
			//执行添加
			$data = I('post.');
			$data['kid'] = $kid;
			//判断如果是编辑
			//手册动态查询--getFieldByName
			if($id = (new TextContentModel())->getFieldByKid($kid,'id')){
				//$data[$this->pk]
				$data['id'] = $id;
			}
			$this->store(new TextContentModel(),$data,function(){
				$this->success('操作成功',u('Module/Keywords/lists',['mo'=>MODULE]));
				exit;
			});
		}
	}
	//执行关键词回复的删除
	public function del($kid)
	{
		(new TextContentModel())->where("kid={$kid}")->delete();
		$this->success('操作成功',u('Module/Keywords/lists',['mo'=>MODULE]));
	}
}