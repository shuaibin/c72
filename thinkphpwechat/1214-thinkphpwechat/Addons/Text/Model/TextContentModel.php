<?php namespace Addons\Text\Model;
use Common\Model\BaseModel;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/17
 * Time: 9:54
 */
class TextContentModel extends BaseModel
{
	protected $pk = 'id';
	protected $tableName = 'text_content';
	protected $_validate = [
		['content','require','请输入关键词回复'],
	];
}