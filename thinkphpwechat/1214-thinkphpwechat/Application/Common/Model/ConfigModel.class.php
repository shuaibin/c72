<?php namespace Common\Model;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/14
 * Time: 12:13
 */
class ConfigModel extends BaseModel
{
	protected $pk = 'id';//主键
	protected $tableName = 'config';//数据表
	protected $_validate  = [
		array('system','require','请输入系统配置项！'),
	];
	//添加
	public function store($data)
	{
		$data[$this->pk] = 1;
		return parent::store($data);
	}
}