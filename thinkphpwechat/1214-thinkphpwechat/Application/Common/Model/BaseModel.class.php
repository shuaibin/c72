<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/16
 * Time: 9:54
 */

namespace Common\Model;


use Think\Model;

class BaseModel extends Model
{
	public function store( $data )
	{
		if( $this->create( $data ) ) {
			//验证成功
			$action = isset( $data[ $this->pk ] ) ? 'save' : 'add';
			$res = $this->$action( $data );
			return [ 'state' => 'success' , 'message' => '操作成功' ,'data'=>$res];
		} else {
			//验证失败
			//['valid'=>0,'message'=>'']
			return [ 'state' => 'faild' , 'message' => $this->getError() ];
		}
	}
}