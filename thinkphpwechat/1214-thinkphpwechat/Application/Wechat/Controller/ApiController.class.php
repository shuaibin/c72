<?php namespace Wechat\Controller;

use Addons\Base\Model\BaseTextModel;
use Addons\Text\Model\TextContentModel;
use Common\Controller\BaseController;
use Common\Model\KeywordsModel;
use wechat\WeChat;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 10:11
 */
class ApiController extends BaseController
{
	public function __init()
	{
		( new WeChat() )->valid();
	}

	public function handler()
	{
		//1.关键词回复
		//消息管理模块,关键词回复
		$instance = ( new WeChat() )->instance( 'message' );
		//判断是否是文本消息
		if( $instance->isTextMsg() ) {
			//获取消息内容
			$message = $instance->getMessage();
			//首先查找数据库
			if( $data = ( new KeywordsModel() )->where( "keywords='{$message->Content}'" )->find() ) {
				//回复的内容需要贼text_content表找
				$data = ( new TextContentModel() )->where( "kid={$data['kid']}" )->find();
				//向用户回复消息
				$instance->text( $data['content'] );
			}
		}
		//3.点击菜单拉取消息时的事件推送
		if( $instance->isClickEvent() ) {
			//获取消息内容
			$message = $instance->getMessage();
			//根据点击词在数据库查找，看有没有与之相对应的关键词的回复
			if( $keywords = ( new KeywordsModel() )->where( "keywords='{$message->EventKey}'" )->find() ) {
				//找到对应的关键词
				//在关键词哦回复表中回去对应的回复内容
				if($content = (new TextContentModel())->where("kid={$keywords['kid']}")->find()){
					//说明可以找到对应的回复
					//向用户回复消息
					//$instance->text( var_export($content,true));
					$instance->text( $content['content']);
				}else{
					//说明没有与关键词想对应的回复,回复默认消息
					$default = (new BaseTextModel())->find(1);
					$instance->text( $default['default']);
				}
			}else{
				//着打不对应的关键词，走默认回复
				$default = (new BaseTextModel())->find(1);
				$instance->text( $default['default']);
			}

		}
		//(new \Addons\Base\Processer)->handler();
		//2.关注回复以及默认回复
		$this->responceMessage( 'base' );
	}

	protected function responceMessage( $module = '' )
	{
		$class = 'Addons\\' . ucfirst( $module ) . '\Processer';
		call_user_func_array( [ new $class , 'handler' ] , [ ] );
	}
}