<?php namespace Module\Controller;

use Common\Controller\AdminController;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 10:57
 */
class EntryController extends AdminController
{
	//跳板，调到Addon/插件目录/类/方法
	public function handler()
	{
//		Array
//		(
//			[mo] => Demo//模块
//			[t] => site//类
//			[ac] => show//方法
//		)
		//dd( $_GET );
		//'Addons\Demo\Site'->show()
		//'Addons\Demo\Web'->show()
		$mo = ucfirst( $_GET['mo'] );
		switch( ucfirst( $_GET['t'] ) ) {
			case 'Site':
				//后台
				$cname = 'Addons\\' . $mo . '\Site';
				break;
			case 'Web':
				$cname = 'Addons\\' . $mo . '\Web';
				break;
		}
		return call_user_func_array( [ new $cname , $_GET['ac'] ] , [ ] );
	}
}