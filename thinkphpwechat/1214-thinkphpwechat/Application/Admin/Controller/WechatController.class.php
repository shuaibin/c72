<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 8:45
 */

namespace Admin\Controller;


use Common\Controller\AdminController;
use Common\Model\ConfigModel;

class WechatController extends AdminController
{
	public function set()
	{
		if(IS_POST)
		{
			$this->store(new ConfigModel(),I('post.'));
		}
		//获取旧数据
		$field = m('config')->find(1);
		$this->assign('field',$field);
		$this->display();
	}
}