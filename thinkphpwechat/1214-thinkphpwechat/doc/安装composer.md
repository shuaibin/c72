<h1 style="color:red;font-size:59px;">composer安装</h1>
----------
###类库管理神器Composer
##
	
		> 下载地址：http://www.phpcomposer.com/
###启用镜像服务的方式有两种（http://docs.phpcomposer.com/）：
##
> 1.系统全局配置： 即将配置信息添加到 Composer 的全局配置文件 config.json 中。打开命令行窗口（windows用户）或控制台（Linux、Mac 用户）并执行如下命令（推荐使用）：
	
		composer config -g repo.packagist composer https://packagist.phpcomposer.com
>2.单个项目配置： 将配置信息添加到某个项目的 composer.json 文件中。修改当前项目的 composer.json 配置文件：
	
	
		1.打开命令行窗口（windows用户）或控制台（Linux、Mac 用户），进入你的项目的根目录（也就是composer.json 文件所在目录），执行如下命令：
			composer config repo.packagist composer https://packagist.phpcomposer.com	

		2.上述命令将会在当前项目中的 composer.json文件的末尾自动添加镜像的配置信息（你也可以自己手工添加）：
			"repositories": {
	       		"packagist": {
	        	"type": "composer",
	        	"url": "https://packagist.phpcomposer.com"
	       		}
	    	}
