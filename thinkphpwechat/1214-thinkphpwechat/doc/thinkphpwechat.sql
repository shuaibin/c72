/*
Navicat MySQL Data Transfer

Source Server         : 75期测试无服务
Source Server Version : 50548
Source Host           : c75_wubin.houdunphp.com:3306
Source Database       : c70_wubin

Target Server Type    : MYSQL
Target Server Version : 50548
File Encoding         : 65001

Date: 2016-12-18 11:13:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `c72_text_content`
-- ----------------------------
DROP TABLE IF EXISTS `c72_text_content`;
CREATE TABLE `c72_text_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(200) DEFAULT NULL COMMENT '关键词回复内容',
  `kid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_text_content
-- ----------------------------
INSERT INTO `c72_text_content` VALUES ('1', '今天真开心', '1');
INSERT INTO `c72_text_content` VALUES ('4', 'houdunwang', '4');
INSERT INTO `c72_text_content` VALUES ('5', '后盾网js视频教程', '5');

-- ----------------------------
-- Table structure for `c72_news_cate`
-- ----------------------------
DROP TABLE IF EXISTS `c72_news_cate`;
CREATE TABLE `c72_news_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cname` varchar(200) DEFAULT '' COMMENT '栏目名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_news_cate
-- ----------------------------
INSERT INTO `c72_news_cate` VALUES ('1', 'xinwen 修改');

-- ----------------------------
-- Table structure for `c72_news_article`
-- ----------------------------
DROP TABLE IF EXISTS `c72_news_article`;
CREATE TABLE `c72_news_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT '' COMMENT '文章标题',
  `content` text COMMENT '文章内容',
  `click` int(10) unsigned DEFAULT '0' COMMENT '点击次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_news_article
-- ----------------------------
INSERT INTO `c72_news_article` VALUES ('1', 'asd修改', '<p><img src=\"http://img.baidu.com/hi/jx2/j_0001.gif\"/>sadfhjk<img src=\"http://img.baidu.com/hi/jx2/j_0001.gif\" style=\"white-space: normal;\"/>sadfhjk<img src=\"http://img.baidu.com/hi/jx2/j_0001.gif\" style=\"white-space: normal;\"/>sadfhjk<img src=\"/c72thinkphpwechat/Upload/ueditor/image/20161218/1482029996557261.jpg\" alt=\"1482029996557261.jpg\"/></p>', '25');

-- ----------------------------
-- Table structure for `c72_module`
-- ----------------------------
DROP TABLE IF EXISTS `c72_module`;
CREATE TABLE `c72_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT '' COMMENT '模块名称',
  `name` varchar(200) DEFAULT NULL COMMENT '模块标识',
  `actions` text COMMENT '模块动作',
  `status` tinyint(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_module
-- ----------------------------
INSERT INTO `c72_module` VALUES ('7', '基本功能', 'Base', '[{\"title\":\"基本回复\",\"name\":\"send\"}]', '0');
INSERT INTO `c72_module` VALUES ('10', '文本回复', 'text', '[]', '1');
INSERT INTO `c72_module` VALUES ('11', '菜单管理', 'button', '[{\"title\":\"菜单列表\",\"name\":\"lists\"}]', '0');
INSERT INTO `c72_module` VALUES ('12', '微官网', 'news', '[{\"title\":\"栏目管理\",\"name\":\"category\"},{\"title\":\"文章管理\",\"name\":\"article\"}]', '0');

-- ----------------------------
-- Table structure for `c72_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `c72_keywords`;
CREATE TABLE `c72_keywords` (
  `kid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keywords` varchar(100) DEFAULT NULL COMMENT '关键词',
  `module` varchar(100) DEFAULT NULL COMMENT '模块标识',
  PRIMARY KEY (`kid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_keywords
-- ----------------------------
INSERT INTO `c72_keywords` VALUES ('1', '1', 'Text');
INSERT INTO `c72_keywords` VALUES ('4', '2', 'Text');
INSERT INTO `c72_keywords` VALUES ('5', 'js', 'Text');

-- ----------------------------
-- Table structure for `c72_config`
-- ----------------------------
DROP TABLE IF EXISTS `c72_config`;
CREATE TABLE `c72_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` text,
  `wechat` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_config
-- ----------------------------
INSERT INTO `c72_config` VALUES ('1', '{\"name\":\"后盾人\",\"icp\":\"后盾网\",\"tel\":\"电话：10086\"}', '{\"token\":\"houdunren\",\"appid\":\"wx032600bf08efc932\",\"appsecret\":\"e83c771d14661b9ba347c0fbed0bd291\"}');

-- ----------------------------
-- Table structure for `c72_button`
-- ----------------------------
DROP TABLE IF EXISTS `c72_button`;
CREATE TABLE `c72_button` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '菜单名称',
  `content` text,
  `status` tinyint(10) unsigned DEFAULT '0' COMMENT '公众号是否正在使用，1使用0未使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_button
-- ----------------------------
INSERT INTO `c72_button` VALUES ('1', 'asd修改', '{\"button\":[{\"type\":\"click\",\"name\":\"今日歌曲\",\"key\":\"V1001_TODAY_MUSIC\"}]}', '0');
INSERT INTO `c72_button` VALUES ('2', '你好菜单', '{\"button\":[{\"type\":\"click\",\"name\":\"今日歌曲\",\"key\":\"V1001_TODAY_MUSIC\"},{\"type\":\"click\",\"name\":\"菜单\",\"sub_button\":[{\"type\":\"view\",\"name\":\"搜索\",\"url\":\"http://www.soso.com/\"},{\"type\":\"view\",\"name\":\"视频\",\"url\":\"http://v.qq.com/\"},{\"type\":\"click\",\"name\":\"赞一下我们\",\"key\":\"V1001_GOOD\"}]}]}', '0');
INSERT INTO `c72_button` VALUES ('4', '后盾网', '{\"button\":[{\"type\":\"view\",\"name\":\"后盾网\",\"key\":\"\",\"url\":\"http://www.baidu.com\"},{\"type\":\"view\",\"name\":\"视频\",\"key\":\"\",\"url\":\"http://www.baidu.com\",\"sub_button\":[{\"type\":\"view\",\"name\":\"php视频\",\"key\":\"\",\"url\":\"http://www.houdunwang.com\"},{\"type\":\"click\",\"name\":\"js\",\"key\":\"js\"}]}]}', '1');

-- ----------------------------
-- Table structure for `c72_base_text`
-- ----------------------------
DROP TABLE IF EXISTS `c72_base_text`;
CREATE TABLE `c72_base_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` varchar(200) DEFAULT '' COMMENT '//系统关注回复',
  `default` varchar(200) DEFAULT '' COMMENT '//默认回复',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_base_text
-- ----------------------------
INSERT INTO `c72_base_text` VALUES ('1', '欢迎关注后盾人武斌', '我不明白你说的是什么...');
