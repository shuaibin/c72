<h2 style='color:red;font-size:35px'>thinkphp之微信开发</h2>

##文档书写
>markdownpad

>看云  http://www.kancloud.cn
##配置服务器
>在服务器wwwroot目录下面新建thinkphpwechat目录

>将thinkphp传到服务器

>在浏览器输入网址：显示框架欢迎语

##下载安装hdjs和后盾微信SDK
>安装nodejs-->npm install hdjs

>安装composer -> composer require houdunwang/wechat


##访问Addons/Article/Site.php
>需要在composer.json文件最后面加上

	"autoload":{
        "psr-4":{
            "Addons\\":"Addons"
        }
    }
>在单入口文件加上

	require 'vendor/autoload.php';
>执行命令

	composer dumpautoload

##记载首页模板页面

	1. <script src="__ROOT__/node_modules/hdjs/require.js"></script>
	2. <script src="__ROOT__/node_modules/hdjs/config.js"></script>
	3. 需要修改index.html
		//配置后台地址
        var hdjs = {
            'base': '__ROOT__/node_modules/hdjs',
            'ueditor':'',
            'uploader': '/admin/Component/uploader',
            'filesLists': '/admin/Component/filesLists',
            'removeImage': '删除图片后台地址'
        };

<h2 style='color:red;font-size:15px'>2016年12月15日09:47:43星期四</h2>
>公众号配置修改

	1.在控制器中执行的程序跟网站配置一样
	2.代码复用，新建Common/Controller/AdminController.class.php
	3.公用的添加代码，放在了AdminController.class.php文件中store方法
>行为【手册：扩展--行为扩展】

	1.Common\Conf\tags.php配置文件定义：
		return [
			'app_begin'=>['Common\\Behaviors\\AppBeginBehavior']
		];
	2.创建了Common/Behaviors/AppBeginBehavior.class.php
		class AppBeginBehavior extends Behavior
		{
			//行为执行入口
			public function run(&$param){
				
			}
		}
	3.创建了Common/Common/helper.php自定义函数库，dd打印函数，全局v函数
	4.Common/Behaviors/AppBeginBehavior.class.php文件将help.php文件进行了加载【加载文件路径】

>创建了前后台公用的Common/Controller/BaseController.class.php

	1.该类继承think的controller类
	2.AdminController继承该类

>在Common/Controller/BaseController.class.php定义了__init

	1.相当于构造方法，执行的时候不会覆盖父级构造方法
		public function __construct(){
			parent::__construct();
			if(method_exists($this,'__init')){
				$this->__init();
			}
		}

>在单入口定义了常量：define('RUNTIME_PATH','Runtime/');

	1.修改了框架运行时Runtime目录


>测试微信绑定【<span style='color:red'>后盾微信SDK中微信类是Wechat，千万注意命名空间导入</span>】

	1.在行为中增加了对微信配置【token】
		$config = [
			//微信首页验证时使用的token http://mp.weixin.qq.com/wiki/8/f9a0b8382e0b77d87b3bcc1ce6fbc104.html
			"token"          => v('config.wechat.token'),
			//公众号身份标识
			"appid"          => v('config.wechat.appid'),
			//公众平台API(参考文档API 接口部分)的权限获取所需密钥Key
			"appsecret"      => v('config.wechat.appsecret'),

		];
		//特别注意导入Wechat类的命名空间
		new WeChat( $config );
	2.在浏览器可以正常访问到Wechat/Controller/handler方法
	3.把2步地址黏贴到微信绑定的url里面【注意我们后台token保持与微信token一致】
	

>访问不同模块【Adons里面不同的插件方法】

	1.不能直接方法Addons目录里面的代码
	2.在Application里面写了一个跳板【Module/Controller/EntryController.class-->handler方法】，完成这个功能
		|--通过区别不同的get参数来识别访问的是谁【注意避开框架的参数】【mo：模块；ac:方法；t：前台/后台】
			$mo = ucfirst( $_GET['mo'] );
			switch( ucfirst( $_GET['t'] ) ) {
				case 'Site':
					//后台
					$cname = 'Addons\\' . $mo . '\Site';
					break;
				case 'Web':
					$cname = 'Addons\\' . $mo . '\Web';
					break;
			}
			return call_user_func_array( [ new $cname , $_GET['ac'] ] , [ ] );

>修改框架默认访问的模块/控制器/方法

	1.Common/conf.php 把默认访问模块/控制器/方法进行修改，指向Module/Entry/handler
	2.这样可以：http://c70_wubin.52tty.com/c72thinkphpwechat/index.php?mo=Demo&t=Site&ac=show。访问我们自定义的模块
>创建自定义跳转函数site_url和web_url

	1.Common/Common/helper.php
		//$url跳转地址   $args：跳转get参数
		function site_url( $url , $args = [ ] )
		{
			$info = explode('.',$url);
			return __APP__ . '?mo='.$info[0].'&ac='.$info[1].'&t=Site&' . http_build_query($args);
		}
>模块的设计

	1.新建Module/Controller/ModuleController.class.php
		|--design方法
			|--创建基本目录机构以及文件
				|--准备模板文件
				|--扫描模板文件中的内容。替换对应的命名空间
				|--重新生成新文件【Addons/所创建的模块的标识/Site.php】