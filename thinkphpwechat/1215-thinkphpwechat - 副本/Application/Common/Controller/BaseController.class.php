<?php
/**
 * 前后台都可以公用
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 9:55
 */

namespace Common\Controller;


use Think\Controller;

class BaseController extends Controller
{
	public function __construct(){
		parent::__construct();
		if(method_exists($this,'__init')){
			$this->__init();
		}
	}
	public function store(Model $model,$data)
	{
		$res = $model->store($data);
		$this->message($res);
	}
	//消息提示
	public function message($res)
	{
		if($res['state']=='success'){
			$this->success($res['message']);
			exit;
		}else{
			$this->error($res['message']);
			exit;
		}
	}
}