<?php namespace Common\Model;
use Think\Model;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/14
 * Time: 12:13
 */
class ConfigModel extends Model
{
	protected $pk = 'id';//主键
	protected $tableName = 'config';//数据表
	protected $_validate  = [
		array('system','require','请输入系统配置项！'),
	];
	//添加
	public function store($data)
	{
		$data[$this->pk] = 1;
		if($this->create($data)){
			//验证成功
			$action = isset($data[$this->pk]) ? 'save' : 'add';
			$this->$action($data);
			return ['state'=>'success','message'=>'操作成功'];
		}else{
			//验证失败
			//['valid'=>0,'message'=>'']
			return ['state'=>'faild','message'=>$this->getError()];
		}
	}
}