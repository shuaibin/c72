<?php  namespace Wechat\Controller;
use Common\Controller\BaseController;
use wechat\WeChat;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 10:11
 */
class ApiController extends BaseController
{
	public function __init()
	{
		(new WeChat())->valid();
	}
	public function handler(){
		//消息管理模块
		$instance = (new WeChat())->instance('message');
		//判断是否是文本消息
		if ($instance->isTextMsg())
		{
			//获取消息内容
			$message = $instance->getMessage();
			//向用户回复消息
			$instance->text('后盾网收到你的消息了...:' . $message->Content);
		}

	}
}