<?php
/**
 * 模块管理
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 12:01
 */

namespace Module\Controller;


use Common\Controller\BaseController;

class ModuleController extends BaseController
{
	public function design()
	{
		if(IS_POST)
		{
			//1.创建基本的目录以及文件
				//$_POST['name']模块表示【模块文件夹名】
				mkdir('Addons/' . ucfirst($_POST['name']) . '/View',0777,true);
				//创建site.php+web.php文件
				foreach(glob('Resource/Data/*.php') as $f){
					//获取文件内容
					$info = file_get_contents($f);
					//替换命名空间
					$content = str_replace('{NAME}',ucfirst($_POST['name']),$info);
					//生成文件
					$file = "Addons/" . ucfirst($_POST['name']) . '/' .basename($f);
					file_put_contents($file,$content);
					//dd($content);
				}
			dd($_POST);die;
		}
		//显示模板文件
		$this->display();
	}
}