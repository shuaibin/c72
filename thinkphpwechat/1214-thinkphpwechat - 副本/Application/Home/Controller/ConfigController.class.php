<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/14
 * Time: 11:33
 */

namespace Home\Controller;


use Common\Model\ConfigModel;
use Think\Controller;

class ConfigController extends Controller
{
	public function set()
	{
		//测试链接数据库
			//$data = m('user')->select();
			//print_r($data);
		//创建wx_config数据表
		//id system
		if(IS_POST)
		{
			//print_r(I('post.'));die;
			//print_r($_POST);exit;
			$configModel = new ConfigModel();
			$res = $configModel->store(I('post.'));
			if($res['state']=='success'){
				$this->success($res['message']);
				exit;
			}else{
				$this->error($res['message']);
				exit;
			}
		}
		//获取数据
		$field = m('config')->find(1);
		$this->assign('field',$field);
		$this->display();
	}
}