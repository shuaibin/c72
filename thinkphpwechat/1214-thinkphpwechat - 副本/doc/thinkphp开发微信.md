<h2 style='color:red;font-size:35px'>thinkphp之微信开发</h2>

##文档书写
>markdownpad

>看云  http://www.kancloud.cn
##配置服务器
>在服务器wwwroot目录下面新建thinkphpwechat目录

>将thinkphp传到服务器

>在浏览器输入网址：显示框架欢迎语

##下载安装hdjs和后盾微信SDK
>安装nodejs-->npm install hdjs

>安装composer -> composer require houdunwang/wechat


##访问Addons/Article/Site.php
>需要在composer.json文件最后面加上

	"autoload":{
        "psr-4":{
            "Addons\\":"Addons"
        }
    }
>在单入口文件加上

	require 'vendor/autoload.php';
>执行命令

	composer dumpautoload

##记载首页模板页面

	1. <script src="__ROOT__/node_modules/hdjs/require.js"></script>
	2. <script src="__ROOT__/node_modules/hdjs/config.js"></script>
	3. 需要修改index.html
		//配置后台地址
        var hdjs = {
            'base': '__ROOT__/node_modules/hdjs',
            'ueditor':'',
            'uploader': '/admin/Component/uploader',
            'filesLists': '/admin/Component/filesLists',
            'removeImage': '删除图片后台地址'
        };