<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/17
 * Time: 8:54
 */

namespace Module\Controller;


use Common\Controller\AdminController;
use Common\Model\KeywordsModel;

class KeywordsController extends AdminController
{
	protected $textModel;

	public function __init()
	{
		$class = 'Addons\\' . MODULE . '\Keywords';
		$this->textModel = new $class;
	}

	//关键词展示页面
	public function lists()
	{
		//获取所有数据
		$field = ( new KeywordsModel() )->where( "module='" . MODULE . "'" )->select();
		$this->assign( 'field' , $field );
		$this->display();
	}

	//设置关键词
	public function set()
	{
		$kid = I( 'get.kid' );

		if( IS_POST ) {
			$data = I( 'post.' );
			$data['module'] = MODULE;
			if( $kid ) {
				//数据如果有主键kid，说明执行编辑
				$data['kid'] = $kid;
			}
			$this->store( new KeywordsModel() , $data , function ( $res ) {
				//$res会返回三个参数 [state] => success [message] => 操作成功 [data] => 5【添加成功后的自增id】
				//如果你是编辑
				if( I( 'get.kid' ) ) {
					$res['data'] = I( 'get.kid' );
				}
				$this->textModel->submit( $res['data'] );
				//$this->newModule( MODULE , $res['data'] );
			} );
		}
		//获取旧数据
		if( $kid ) {
			$field = ( new KeywordsModel() )->find( $kid );
			$this->assign( 'field' , $field );
			$this->newModule( MODULE , $kid );
		}
		//关键词回复页面
		$content = $this->newModule( 'text' );
		$this->assign( 'content' , $content );
		$this->display();
	}

	//实例化关键词处理模块
	public function newModule( $moduel , $kid = "" )
	{
		$class = 'Addons\\' . ucfirst( $moduel ) . '\Keywords';

		return call_user_func_array( [ new $class , 'set' ] , [ 'kid' => $kid ] );
	}

	//删除关键词
	public function del()
	{
		$kid = I('get.kid');
		(new KeywordsModel())->where("kid={$kid}")->delete();
		$this->textModel->del($kid);
	}
}