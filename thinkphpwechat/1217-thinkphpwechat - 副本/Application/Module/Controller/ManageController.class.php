<?php
/**
 * 模块管理
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 12:01
 */

namespace Module\Controller;


use Common\Controller\AdminController;
use Common\Model\ModuleModel;

class ManageController extends AdminController
{
	//模块列表
	public function lists()
	{
		$data = [ ];
		//获取所有模块数据
		foreach( glob( 'Addons/*' ) as $f ) {
			//检测有没有配置文件
			if( is_file( $manifest = $f . '/manifest.php' ) ) {
				$data[] = include $manifest;
			}
		}
		//获取已经安装的模块
		//$has = ( new ModuleModel() )->getField('name',true);
		$has = ( new ModuleModel() )->getField('id,name');
		$this->assign('has',$has);
		$this->assign( 'data' , $data );
		$this->display();
	}

	//设计模块
	public function design()
	{
		if( IS_POST ) {
			//3.增加模块重复创建的判断
			if( is_dir( 'Addons/' . ucfirst( $_POST['name'] ) ) ) {
				$this->error( '模块已经存在，不允许重复创建' );
				exit;
			}
			//1.创建基本的目录以及文件
			$this->createDir();
			//2.生成配置文件manifest.php
			$this->createManifest();
			$this->success('操作成功','lists');exit;
		}
		//显示模板文件
		$this->display();
	}

	//2.生成配置文件manifest.php
	protected function createManifest()
	{
		//@(\r|\n)@正则,array_filter过滤掉空元素
		$data = array_filter( preg_split( '@(\r|\n)@' , $_POST['actions'] ) );
		//循环进行拆分
		$actions = [ ];
		foreach( $data as $k => $v ) {
			$info = explode( '|' , $v );
			$actions[] = [ 'title' => $info[0] , 'name' => $info[1] ];
		}
		//重新赋值回去，影响$_POST里面的actions
		$_POST['actions'] = $actions;
		//生成配置文件manifest.php
		file_put_contents( 'Addons/' . ucfirst( $_POST['name'] ) . '/manifest.php' , '<?php return ' . var_export( $_POST , true ) . ' ;?>' );
		//dd( $_POST );
	}

	//1.创建基本的目录以及文件
	protected function createDir()
	{
		//$_POST['name']模块表示【模块文件夹名】
		mkdir( 'Addons/' . ucfirst( $_POST['name'] ) . '/View' , 0777 , true );
		//创建site.php+web.php文件
		foreach( glob( 'Resource/Data/*.php' ) as $f ) {
			//获取文件内容
			$info = file_get_contents( $f );
			//替换命名空间
			$content = str_replace( '{NAME}' , ucfirst( $_POST['name'] ) , $info );
			//生成文件
			$file = "Addons/" . ucfirst( $_POST['name'] ) . '/' . basename( $f );
			file_put_contents( $file , $content );
			//dd($content);
		}
	}

	//模块安装
	public function install()
	{
		//name模块标识。目录名字
		//q('get.cid',0,'intval');
		$name = I( 'get.name' , '' , 'ucfirst' );
		//检测不能重复安装
		if( ( new ModuleModel() )->where( "name='{$_GET['name']}'" )->find() ) {
			$this->error( '模块已经安装,请不要重复安装' );
			exit;
		}
		//读取配置文件内容
		$content = include 'Addons/' . $name . '/manifest.php';
		$content['actions'] = json_encode( $content['actions'] , JSON_UNESCAPED_UNICODE );
		$this->store( new ModuleModel() , $content );

	}

	//模块卸载
	public function uninstall()
	{
		if( ( new ModuleModel() )->where( "name='{$_GET['name']}'" )->delete() ) {
			$this->success( '操作成功' );
			exit;
		} else {
			$this->error( '操作失败' );
			exit;
		}
	}
}