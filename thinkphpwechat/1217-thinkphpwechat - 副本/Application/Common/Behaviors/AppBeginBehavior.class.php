<?php namespace Common\Behaviors;
use Common\Model\ConfigModel;
use Think\Behavior;
use wechat\WeChat;

require 'Application/Common/Common/helper.php';
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 9:13
 */
class AppBeginBehavior extends Behavior
{
	//行为执行入口
	public function run(&$param){
		//设置配置
		$this->loadConfig();
		//微信绑定配置
		$this->bindWechatConfig();
		//定义常量
			/*$d = get_defined_constants(true);
			dd($d['user']);*/
		define('MODULE',ucfirst($_GET['mo']));
		define('ACTION',$_GET['ac']);
	}
	//微信绑定
	public function bindWechatConfig()
	{
		$config = [
			//微信首页验证时使用的token http://mp.weixin.qq.com/wiki/8/f9a0b8382e0b77d87b3bcc1ce6fbc104.html
			"token"          => v('config.wechat.token'),
			//公众号身份标识
			"appid"          => v('config.wechat.appid'),
			//公众平台API(参考文档API 接口部分)的权限获取所需密钥Key
			"appsecret"      => v('config.wechat.appsecret'),

		];
		new WeChat( $config );
	}
	//设置系统配置和微信配置
	public function loadConfig()
	{
		$configData = (new ConfigModel())->find(1);
		$configData['system'] = json_decode($configData['system'],true);
		$configData['wechat'] = json_decode($configData['wechat'],true);
		//将配置存入全局函数v
		v('config.system',$configData['system']);
		v('config.wechat',$configData['wechat']);
	}
}