<?php namespace Common\Controller;

use Common\Model\ModuleModel;
use Think\Controller;

/**
 * 后台公共控制器
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 8:54
 */
class AdminController extends BaseController
{
	//_moduleData,系统分配变量
	public function __construct()
	{
		parent::__construct();
		$moduleData = ( new ModuleModel() )->select();
		foreach( $moduleData as $k => $v ) {
			$moduleData[$k]['actions'] = json_decode( $v['actions'] , true );
		}

		//dd( $moduleData );
		$this->assign( '_moduleData' , $moduleData );
	}
}