<?php
/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/17
 * Time: 9:17
 */

namespace Common\Model;


class KeywordsModel extends BaseModel
{
	protected $pk='kid';
	protected $tableName ='keywords';
	protected $_validate = [
		['keywords','require','请输入关键词'],
	];
}