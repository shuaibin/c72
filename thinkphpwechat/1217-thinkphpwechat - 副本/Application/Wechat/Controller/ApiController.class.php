<?php namespace Wechat\Controller;

use Addons\Text\Model\TextContentModel;
use Common\Controller\BaseController;
use Common\Model\KeywordsModel;
use wechat\WeChat;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 10:11
 */
class ApiController extends BaseController
{
	public function __init()
	{
		( new WeChat() )->valid();
	}

	public function handler()
	{
		//关键词回复
		//消息管理模块
		$instance = (new WeChat())->instance('message');
		//判断是否是文本消息
		if ($instance->isTextMsg())
		{
			//获取消息内容
			$message = $instance->getMessage();
			//首先查找数据库
			if($data = (new KeywordsModel())->where("keywords='{$message->Content}'")->find()){
				//回复的内容需要贼text_content表找
				$data = (new TextContentModel())->where("kid={$data['kid']}")->find();
				//向用户回复消息
				$instance->text($data['content']);
			}
		}
		//(new \Addons\Base\Processer)->handler();
		//关注回复以及默认回复
		$this->responceMessage( 'base' );
	}

	protected function responceMessage( $module = '' )
	{
		$class = 'Addons\\' . ucfirst($module) . '\Processer';
		call_user_func_array( [ new $class , 'handler' ] , [ ] );
	}
}