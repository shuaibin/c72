<?php
/**
 * 微信处理器
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/16
 * Time: 12:01
 */

namespace Addons\Base;


use Addons\Base\Model\BaseTextModel;
use Addons\HdProcesser;

class Processer extends HdProcesser
{
	public function handler()
	{
		//获取数据库内容数据
		$data = (new BaseTextModel())->find();
		//关注回复消息
		//判断是否是关注事件
		if ($this->message->isSubscribeEvent())
		{
			//向用户回复消息
			$this->message->text($data['system']);
		}
		//默认消息
//判断是否是文本消息
		if ($this->message->isTextMsg())
		{
			//获取消息内容
			$message = $this->message->getMessage();
			//向用户回复消息
			$this->message->text($data['default']);
		}

	}
}