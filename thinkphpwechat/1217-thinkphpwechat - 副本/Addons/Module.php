<?php namespace Addons;

use Common\Controller\BaseController;

/**
 * 模块的公共控制器
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/15
 * Time: 10:53
 */
class Module extends BaseController
{
	public function make( $tpl = '' )
	{
		//'Addons/Base/View/send.html'
		$name = $tpl ? $tpl : ACTION;
		$this->display('Addons/'.MODULE.'/View/'.$name.'.html');
	}
}