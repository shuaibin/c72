<?php namespace Addons\Button;
use Addons\Button\Model\ButtonModel;
use Addons\Module;

/**
 * 后台
 * Created by PhpStorm.
 * User: 武斌
 * Date: 2016/12/14
 * Time: 10:45
 */
class Site extends Module
{
	public function lists()
	{
		$this->make();
	}
	//菜单添加
	public function addButton()
	{
		if(IS_POST){
			$this->store(new ButtonModel(),I('post.'));
		}
		$this->make();
	}
}