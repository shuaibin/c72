<?php namespace Addons\Button\Model;
use Common\Model\BaseModel;

/**
 * Created by PhpStorm.
 * User: 武 斌
 * Date: 2016/12/17
 * Time: 12:31
 */
class ButtonModel extends BaseModel
{
	protected $pk = 'id';
	protected $tableName = 'button';
	protected $_validate = [
		['title','require','请输入菜单名称'],
		['content','require','请填写菜单内容'],
	];
}