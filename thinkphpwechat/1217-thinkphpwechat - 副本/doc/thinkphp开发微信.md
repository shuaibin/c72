<h2 style='color:red;font-size:35px'>thinkphp之微信开发</h2>

##文档书写
>markdownpad

>看云  http://www.kancloud.cn
##配置服务器
>在服务器wwwroot目录下面新建thinkphpwechat目录

>将thinkphp传到服务器

>在浏览器输入网址：显示框架欢迎语

##下载安装hdjs和后盾微信SDK
>安装nodejs-->npm install hdjs

>安装composer -> composer require houdunwang/wechat


##访问Addons/Article/Site.php
>需要在composer.json文件最后面加上

	"autoload":{
        "psr-4":{
            "Addons\\":"Addons"
        }
    }
>在单入口文件加上

	require 'vendor/autoload.php';
>执行命令

	composer dumpautoload

##记载首页模板页面

	1. <script src="__ROOT__/node_modules/hdjs/require.js"></script>
	2. <script src="__ROOT__/node_modules/hdjs/config.js"></script>
	3. 需要修改index.html
		//配置后台地址
        var hdjs = {
            'base': '__ROOT__/node_modules/hdjs',
            'ueditor':'',
            'uploader': '/admin/Component/uploader',
            'filesLists': '/admin/Component/filesLists',
            'removeImage': '删除图片后台地址'
        };

<h2 style='color:red;font-size:15px'>2016年12月15日09:47:43星期四</h2>
>公众号配置修改

	1.在控制器中执行的程序跟网站配置一样
	2.代码复用，新建Common/Controller/AdminController.class.php
	3.公用的添加代码，放在了AdminController.class.php文件中store方法
>行为【手册：扩展--行为扩展】

	1.Common\Conf\tags.php配置文件定义：
		return [
			'app_begin'=>['Common\\Behaviors\\AppBeginBehavior']
		];
	2.创建了Common/Behaviors/AppBeginBehavior.class.php
		class AppBeginBehavior extends Behavior
		{
			//行为执行入口
			public function run(&$param){
				
			}
		}
	3.创建了Common/Common/helper.php自定义函数库，dd打印函数，全局v函数
	4.Common/Behaviors/AppBeginBehavior.class.php文件将help.php文件进行了加载【加载文件路径】

>创建了前后台公用的Common/Controller/BaseController.class.php

	1.该类继承think的controller类
	2.AdminController继承该类

>在Common/Controller/BaseController.class.php定义了__init

	1.相当于构造方法，执行的时候不会覆盖父级构造方法
		public function __construct(){
			parent::__construct();
			if(method_exists($this,'__init')){
				$this->__init();
			}
		}

>在单入口定义了常量：define('RUNTIME_PATH','Runtime/');

	1.修改了框架运行时Runtime目录


>测试微信绑定【<span style='color:red'>后盾微信SDK中微信类是Wechat，千万注意命名空间导入</span>】

	1.在行为中增加了对微信配置【token】
		$config = [
			//微信首页验证时使用的token http://mp.weixin.qq.com/wiki/8/f9a0b8382e0b77d87b3bcc1ce6fbc104.html
			"token"          => v('config.wechat.token'),
			//公众号身份标识
			"appid"          => v('config.wechat.appid'),
			//公众平台API(参考文档API 接口部分)的权限获取所需密钥Key
			"appsecret"      => v('config.wechat.appsecret'),

		];
		//特别注意导入Wechat类的命名空间
		new WeChat( $config );
	2.在浏览器可以正常访问到Wechat/Controller/handler方法
	3.把2步地址黏贴到微信绑定的url里面【注意我们后台token保持与微信token一致】
	

>访问不同模块【Adons里面不同的插件方法】

	1.不能直接方法Addons目录里面的代码
	2.在Application里面写了一个跳板【Module/Controller/EntryController.class-->handler方法】，完成这个功能
		|--通过区别不同的get参数来识别访问的是谁【注意避开框架的参数】【mo：模块；ac:方法；t：前台/后台】
			$mo = ucfirst( $_GET['mo'] );
			switch( ucfirst( $_GET['t'] ) ) {
				case 'Site':
					//后台
					$cname = 'Addons\\' . $mo . '\Site';
					break;
				case 'Web':
					$cname = 'Addons\\' . $mo . '\Web';
					break;
			}
			return call_user_func_array( [ new $cname , $_GET['ac'] ] , [ ] );

>修改框架默认访问的模块/控制器/方法

	1.Common/conf.php 把默认访问模块/控制器/方法进行修改，指向Module/Entry/handler
	2.这样可以：http://c70_wubin.52tty.com/c72thinkphpwechat/index.php?mo=Demo&t=Site&ac=show。访问我们自定义的模块
>创建自定义跳转函数site_url和web_url

	1.Common/Common/helper.php
		//$url跳转地址   $args：跳转get参数
		function site_url( $url , $args = [ ] )
		{
			$info = explode('.',$url);
			return __APP__ . '?mo='.$info[0].'&ac='.$info[1].'&t=Site&' . http_build_query($args);
		}
>模块的设计

	1.新建Module/Controller/ModuleController.class.php
		|--design方法
			|--创建基本目录机构以及文件
				|--准备模板文件
				|--扫描模板文件中的内容。替换对应的命名空间
				|--重新生成新文件【Addons/所创建的模块的标识/Site.php】

<h2 style='color:red;font-size:15px'>2016年12月16日09:31:53星期五</h2>
>模块设计

	3.增加模块不能重复设计的判断
	2.生成配置文件manifest.php
		|--将post提交数据写入到文件中
			|--actions:[['title'=>'栏目','name'=>'cate']]
	1.昨天模块设计中1
	4.成功跳转，跳转到列表页
>模块列表

	1.读取所有合法模块数据
		|--Addons下目录【必须包含有配置文件才去读取】
	2.获取已安装的模块，作为lists模板页面的判断
>安装模块【写入数据库】

	1.创建数据库c72_module【id、title、name、actions】
	2.创建对应模型
	3.不能重复安装模块
	4.读取对应配置文件内容，写入数据库，【actions转json】
>模块卸载

	1.删除对应数据库的数据

>公共模板中模块数据

	1.AdminController.class.php进行数据分配【数据：取自数据库】
	2.公共控制器中分配变量时候，区别子控制器中的变量

>创建微信基本功能的模块【删除原测试模块，注意不要删Addons目录下的Module】

	1.创建了Base【基本功能模块】--关注回复以及默认回复
	2.在Base/Processer.php【微信处理器】+ Addons/HdProcesser.php【各个模块公共的微信处理器类】
	3.Wechat/Controller/ApiController->handler()当作跳板，微信基本信息处理放在了Base/Processer.php-->handler方法处理
	4.获取数据库关注回复以及默认回复消息，进行替换

<h2 style='color:red;font-size:15px'>2016年12月17日08:39:48星期六</h2>

>修改设计模块页面

	1.增加一个单选框【是否关键词消息】
	2.c72_module数据库增加status 0不是关键词消息【默认】 1关键词消息
>设计关键词模块
>关键词中动作有两部分组成，中间注意参数传递

	1.是否关键词消息【选择的是】
	2.Application/Module/Controller/KeywordsController.class.php-->lists方法【展示页面】
	3.添加关键词消息【添加、编辑、删除采用两部分组合完成的】
		|--关键词KeywordsController.class.php中的set方法
		|--关键词回复Addons/Text/Kkeywords.php中的submit方法进行的添加
		|--在BaseController.class.php里面store方法增加一个闭包函数，解决执行添加完成之后代码无法继续运行
>关键词编辑

	1.在set方法$kid = I( 'get.kid' );
	2.在if(IS_POST{})，在执行编辑之前增加判断
			if( $kid ) {
				//数据如果有主键kid，说明执行编辑
				$data['kid'] = $kid;
			}	
	3.在Addons/Text/Keywords--submit方法增加判断：
			对于编辑判断：$data['id'] = $id;
			if($id = (new TextContentModel())->getFieldByKid($kid,'id')){
				//$data[$this->pk]
				$data['id'] = $id;
			}
	4.编辑关键词没有问题，但是编辑关键词回复表的时候提示操作成功，但是实际上没有办法正常修改
		|--无法编辑成功，肯定是判断中当前表的主键id有问题
		|--id根据关键词kid来获取的
		|--kid来源：BaseModel.class.php中执行store方法来的：
			|--$action = isset( $data[ $this->pk ] ) ? 'save' : 'add';
			|--$res = $this->$action( $data )返回值
			|--问题出在了$res返回值身上
			|--add添加时候正常返回，没有问题
			|--save编辑时候返回1或者0，不能正常返回网我们要修改的数据的kid值
		|--修改：Applications/Module/Controller/KeywordsController.class.php执行set方法时候增加判断：
			$this->store( new KeywordsModel() , $data , function ( $res ) {
				//$res会返回三个参数 [state] => success [message] => 操作成功 [data] => 5【添加成功后的自增id】
				//如果你是编辑
				if( I( 'get.kid' ) ) {
					$res['data'] = I( 'get.kid' );
				}
				$this->textModel->submit( $res['data'] );
				//$this->newModule( MODULE , $res['data'] );
			} );
			|--这个判断应该这样写：
				if( $kid ) {
					$res['data'] = I( 'get.kid' );
				}
				但是在闭包中，无法使用外部的变量，修改为：
				if( I( 'get.kid' ) ) {
					$res['data'] = I( 'get.kid' );
				}
>关键词的删除

	1.Application/Module/Controller/Keywordscontroller.class.php中执行删除方法删除关键词表对应的数据
	2.Addons/Text/Keywords.php执行删除方法删除关键词回复表的数据

