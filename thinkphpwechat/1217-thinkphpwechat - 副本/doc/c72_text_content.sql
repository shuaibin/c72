/*
Navicat MySQL Data Transfer

Source Server         : 75期测试无服务
Source Server Version : 50548
Source Host           : c75_wubin.houdunphp.com:3306
Source Database       : c70_wubin

Target Server Type    : MYSQL
Target Server Version : 50548
File Encoding         : 65001

Date: 2016-12-17 12:34:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `c72_text_content`
-- ----------------------------
DROP TABLE IF EXISTS `c72_text_content`;
CREATE TABLE `c72_text_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(200) DEFAULT NULL COMMENT '关键词回复内容',
  `kid` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_text_content
-- ----------------------------
INSERT INTO `c72_text_content` VALUES ('1', '今天真开心', '1');
INSERT INTO `c72_text_content` VALUES ('4', 'houdunwang.com', '4');

-- ----------------------------
-- Table structure for `c72_module`
-- ----------------------------
DROP TABLE IF EXISTS `c72_module`;
CREATE TABLE `c72_module` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT '' COMMENT '模块名称',
  `name` varchar(200) DEFAULT NULL COMMENT '模块标识',
  `actions` text COMMENT '模块动作',
  `status` tinyint(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_module
-- ----------------------------
INSERT INTO `c72_module` VALUES ('7', '基本功能', 'Base', '[{\"title\":\"基本回复\",\"name\":\"send\"}]', '0');
INSERT INTO `c72_module` VALUES ('10', '文本回复', 'text', '[]', '1');
INSERT INTO `c72_module` VALUES ('11', '菜单管理', 'button', '[{\"title\":\"菜单列表\",\"name\":\"lists\"}]', '0');

-- ----------------------------
-- Table structure for `c72_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `c72_keywords`;
CREATE TABLE `c72_keywords` (
  `kid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keywords` varchar(100) DEFAULT NULL COMMENT '关键词',
  `module` varchar(100) DEFAULT NULL COMMENT '模块标识',
  PRIMARY KEY (`kid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_keywords
-- ----------------------------
INSERT INTO `c72_keywords` VALUES ('1', '1', 'Text');
INSERT INTO `c72_keywords` VALUES ('4', '2', 'Text');

-- ----------------------------
-- Table structure for `c72_config`
-- ----------------------------
DROP TABLE IF EXISTS `c72_config`;
CREATE TABLE `c72_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` text,
  `wechat` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_config
-- ----------------------------
INSERT INTO `c72_config` VALUES ('1', '{\"name\":\"后盾人\",\"icp\":\"后盾网\",\"tel\":\"电话：10086\"}', '{\"token\":\"houdunren\",\"appid\":\"sdfsdfe273d\",\"appsecret\":\"1c72ad236f72c70e347343653410934b\"}');

-- ----------------------------
-- Table structure for `c72_button`
-- ----------------------------
DROP TABLE IF EXISTS `c72_button`;
CREATE TABLE `c72_button` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '菜单名称',
  `content` text,
  `status` tinyint(10) unsigned DEFAULT '0' COMMENT '公众号是否正在使用，1使用0未使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_button
-- ----------------------------
INSERT INTO `c72_button` VALUES ('1', 'asd', '{\"button\":[{\"type\":\"click\",\"name\":\"今日歌曲\",\"key\":\"V1001_TODAY_MUSIC\"},{\"type\":\"click\",\"name\":\"菜单\",\"sub_button\":[{\"type\":\"view\",\"name\":\"搜索\",\"url\":\"http://www.soso.com/\"},{\"type\":\"view\",\"name\":\"视频\",\"url\":\"http://v.qq.com/\"},{\"type\":\"click\",\"name\":\"赞一下我们\",\"key\":\"V1001_GOOD\"}]}]}', '0');

-- ----------------------------
-- Table structure for `c72_base_text`
-- ----------------------------
DROP TABLE IF EXISTS `c72_base_text`;
CREATE TABLE `c72_base_text` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system` varchar(200) DEFAULT '' COMMENT '//系统关注回复',
  `default` varchar(200) DEFAULT '' COMMENT '//默认回复',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of c72_base_text
-- ----------------------------
INSERT INTO `c72_base_text` VALUES ('1', '欢迎关注后盾人武斌', '我不明白你说的是什么...');
